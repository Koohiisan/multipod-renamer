﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace MultiPodRenamer
{
    public partial class Form1 : Form
    {
        public string file = "";
        public string nameItem = "";
        public string newname = "";
        public string ext = "";
        public string[] files = null;
        public DirectoryInfo dir = null;
        public FileInfo[] infos = null;
        public string[,] xmllist = new string[20000,2];
        public Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
        int counter = 0;

        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                dir = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
                infos = dir.GetFiles();
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            openFileDialog1.Filter = "XML Files (.xml)|*.xml";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                file = openFileDialog1.FileName;
                try
                {
                    xmlreader();
                    for(int i = 0; i< counter; i++)
                    {
                        listBox1.Items.Add(xmllist[i,0] + " --- " + xmllist[i,1]);
                    }
                }
                catch (IOException)
                {
                }
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (file == "" || dir == null)
                MessageBox.Show("Please select a folder and xml file first", "Error");
            else
            {
                listBox1.Items.Clear();

                foreach (FileInfo f in infos)
                {
                    ext = f.Extension;
                    for (int i = 0; i < counter; i++)
                    {
                        if (f.Name == xmllist[i, 0])
                        {
                            newname = f.DirectoryName + "\\" + xmllist[i, 1] + ext;
                            newname = newname.Replace("\t", String.Empty);
                            newname = newname.Replace("\n", String.Empty);
                            newname = newname.Replace("\r", String.Empty);
                            try
                            {
                                File.Move(f.FullName, newname);
                            }
                            catch (System.IO.FileNotFoundException) 
                            {
                                listBox1.Items.Add("Error (file not found): " + f.FullName);
                            }
                        }
                    }
                }
                MessageBox.Show("Completed","Completed");
            }
        }

        private void xmlreader()
        {
// Start with XmlReader object

            //here, we try to setup Stream between the XML file nad xmlReader

            using (XmlReader reader = XmlReader.Create(file))
            {
                counter = 0;
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        nameItem = reader.Name.ToString();
                            if (nameItem.Equals("title"))
                            {
                                string myString = illegalInFileName.Replace(reader.ReadString(), "");
                                xmllist[counter,1] = myString.Trim();
                            }
                            if (nameItem.Equals("enclosure"))
                            {
                                string attribute = reader["url"];
                                if (attribute != null)
                                {
                                    xmllist[counter, 0] = attribute.Substring(attribute.LastIndexOf("/") + 1).Trim();
                                }
                                counter++;
                            }
                    }
                }
            }
        }
    }
}
